package com.fullclient.bemobi.data.remote.model;

import com.fullclient.bemobi.model.AppModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by medal on 3/16/2018.
 */

public class Apps {

    @SerializedName("apps")
    @Expose
    private List<AppModel> apps = null;

    public Apps(){

    }

    public Apps(List<AppModel> apps) {
        this.apps = apps;
    }

    public void setApps(List<AppModel> apps) {
        this.apps = apps;
    }

    public List<AppModel> getApps() {
        return apps;
    }
}
