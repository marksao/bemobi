package com.fullclient.bemobi.data;

import com.fullclient.bemobi.data.remote.AppsRestService;
import com.fullclient.bemobi.model.AppModel;
import com.fullclient.bemobi.model.Device;

import java.io.IOException;
import java.util.List;

import rx.Observable;

/**
 * Created by Gabriel on 08/07/2017.
 */

public class AppsRepositoryImpl implements AppsRepository {

    private AppsRestService appsRestService;

    public AppsRepositoryImpl(AppsRestService appsRestService) {
        this.appsRestService = appsRestService;
    }

    @Override
    public Observable<List<AppModel>> listApps(Device device) {
        return Observable.defer(() -> appsRestService.listApps(device).concatMap(
                call -> Observable.from(call.getApps()).toList()))
                .retryWhen(observable -> observable.flatMap(o -> {
                    if (o instanceof IOException) {
                        return Observable.just(null);
                    }
                    return Observable.error(o);
                }));
    }
}
