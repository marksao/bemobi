package com.fullclient.bemobi.data;

import com.fullclient.bemobi.model.AppModel;
import com.fullclient.bemobi.model.Device;

import java.util.List;

import rx.Observable;

/**
 * Created by Gabriel on 08/07/2017.
 */

public interface AppsRepository {
    Observable<List<AppModel>> listApps(Device device);
}
