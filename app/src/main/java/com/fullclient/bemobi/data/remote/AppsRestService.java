package com.fullclient.bemobi.data.remote;

import com.fullclient.bemobi.data.remote.model.Apps;
import com.fullclient.bemobi.model.Device;

import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by Gabriel on 08/07/2017.
 */

public interface AppsRestService {
    @POST("app")
    Observable<Apps> listApps(@Body Device device);
}
