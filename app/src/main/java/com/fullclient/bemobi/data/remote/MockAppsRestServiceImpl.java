package com.fullclient.bemobi.data.remote;

import com.fullclient.bemobi.data.remote.model.Apps;
import com.fullclient.bemobi.model.AppModel;
import com.fullclient.bemobi.model.Device;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

/**
 * Created by Gabriel on 10/07/2017.
 */

public class MockAppsRestServiceImpl implements AppsRestService {

    private Apps app = new Apps();
    private static Observable listAppsResult = null;

    public MockAppsRestServiceImpl() {
        List<AppModel> apps = new ArrayList<>();
        apps.add(app1());
        apps.add(app2());
        app.setApps(apps);
    }

    @Override
    public Observable<Apps> listApps(Device device) {
        if (listAppsResult != null) {
            return listAppsResult;
        }
        return Observable.just(app);
    }

    private AppModel app1(){
        AppModel app = new AppModel();
        app.setName("");
        return app;
    }

    private AppModel app2(){
        AppModel app = new AppModel();
        app.setName("");
        return app;
    }

    public static void setListAppsResult(Observable list) {
        listAppsResult = list;
    }


}
