package com.fullclient.bemobi.injection;

import com.fullclient.bemobi.data.AppsRepository;
import com.fullclient.bemobi.data.AppsRepositoryImpl;
import com.fullclient.bemobi.data.remote.AppsRestService;
import com.fullclient.bemobi.data.remote.MockAppsRestServiceImpl;


/**
 * Created by Gabriel on 10/07/2017.
 */

public class InjectionMock {

    private static AppsRestService appsRestService;

    public static AppsRepository provideListApps() {
        return new AppsRepositoryImpl(provideAppsRestService());
    }

    static AppsRestService provideAppsRestService() {
        if (appsRestService == null) {
            appsRestService = new MockAppsRestServiceImpl();
        }
        return appsRestService;
    }

}
