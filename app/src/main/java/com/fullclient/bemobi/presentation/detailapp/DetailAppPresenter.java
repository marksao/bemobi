package com.fullclient.bemobi.presentation.detailapp;


import com.fullclient.bemobi.model.AppModel;
import com.fullclient.bemobi.presentation.base.BasePresenter;


/**
 * Created by Gabriel on 08/07/2017.
 */

public class DetailAppPresenter extends BasePresenter<DetailAppContract.View> implements DetailAppContract.Presenter {

    @Override
    public void showApp(AppModel app) {
        getView().showResult(app);
    }
}