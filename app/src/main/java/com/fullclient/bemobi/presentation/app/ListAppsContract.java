package com.fullclient.bemobi.presentation.app;


import com.fullclient.bemobi.model.AppModel;
import com.fullclient.bemobi.model.Device;
import com.fullclient.bemobi.presentation.base.MvpPresenter;
import com.fullclient.bemobi.presentation.base.MvpView;

import java.util.List;

/**
 * Created by Gabriel on 08/07/2017.
 */

public interface ListAppsContract {
    interface View extends MvpView {
        void remountResults(List<AppModel> apps);

        void showResults(List<AppModel> apps);

        void showError(String message);

        void showLoading();

        void hideLoading();
    }

    interface Presenter extends MvpPresenter<View> {
        void listApps(Device device);

        void remountListApps(List<AppModel> apps);
    }
}
