package com.fullclient.bemobi.presentation.detailapp;

import com.fullclient.bemobi.model.AppModel;
import com.fullclient.bemobi.presentation.base.MvpPresenter;
import com.fullclient.bemobi.presentation.base.MvpView;


/**
 * Created by Gabriel on 08/07/2017.
 */

public interface DetailAppContract {
    interface View extends MvpView {
        void showResult(AppModel appModel);
    }

    interface Presenter extends MvpPresenter<View> {
        void showApp(AppModel appModel);
    }
}
