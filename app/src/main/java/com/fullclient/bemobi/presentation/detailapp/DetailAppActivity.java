package com.fullclient.bemobi.presentation.detailapp;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fullclient.bemobi.R;
import com.fullclient.bemobi.model.AppModel;
import com.squareup.picasso.Picasso;


public class DetailAppActivity extends AppCompatActivity implements DetailAppContract.View {

    public static final String EXTRA_CIRCULAR_REVEAL_X = "EXTRA_CIRCULAR_REVEAL_X";
    public static final String EXTRA_CIRCULAR_REVEAL_Y = "EXTRA_CIRCULAR_REVEAL_Y";

    View rootLayout;

    private int revealX;
    private int revealY;

    private TextView tv_name_app;
    private TextView tv_version_app;
    private LinearLayout bd_color;
    private ImageView img_app;
    private TextView tv_subtitle;
    private AppModel model;
    private DetailAppPresenter detailAppPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_app);
        detailAppPresenter = new DetailAppPresenter();
        detailAppPresenter.attachView(this);

        final Intent intent = getIntent();

        rootLayout = findViewById(R.id.rootLayout);

        tv_name_app = findViewById(R.id.tv_app_name);
        tv_version_app = findViewById(R.id.tv_version);
        img_app = findViewById(R.id.ico);
        tv_subtitle = findViewById(R.id.tv_subtitle);

        if(getActionBar()!=null)getActionBar().setDisplayHomeAsUpEnabled(true);
        if(getSupportActionBar()!=null)getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        model = (AppModel) intent.getSerializableExtra("app");

        if (savedInstanceState == null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP &&
                intent.hasExtra(EXTRA_CIRCULAR_REVEAL_X) &&
                intent.hasExtra(EXTRA_CIRCULAR_REVEAL_Y)) {

            rootLayout.setVisibility(View.INVISIBLE);

            revealX = intent.getIntExtra(EXTRA_CIRCULAR_REVEAL_X, 0);
            revealY = intent.getIntExtra(EXTRA_CIRCULAR_REVEAL_Y, 0);

            ViewTreeObserver viewTreeObserver = rootLayout.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        revealActivity(revealX, revealY);
                        rootLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });
            }

        } else {
            rootLayout.setVisibility(View.VISIBLE);
        }

        detailAppPresenter.showApp(model);
    }

    @Override
    public void showResult(AppModel app) {
        tv_subtitle.setText(String.format("%s",app.getSubtitle()));
        tv_name_app.setText(String.format("%s", app.getName()));
        tv_version_app.setText(String.format("%s", app.getVersion()));
        Picasso.get().load(app.getIcon()).into(img_app);
        rootLayout.setBackgroundColor(Color.parseColor(app.getColor()));
        //bd_color.setBackgroundColor(Color.parseColor(app.color));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        detailAppPresenter.detachView();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        unRevealActivity();
    }

    protected void revealActivity(int x, int y) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            float finalRadius = (float) (Math.max(rootLayout.getWidth(), rootLayout.getHeight()) * 1.1);

            // create the animator for this view (the start radius is zero)
            Animator circularReveal = ViewAnimationUtils.createCircularReveal(rootLayout, x, y, 0, finalRadius);
            circularReveal.setDuration(400);
            circularReveal.setInterpolator(new AccelerateInterpolator());

            // make the view visible and start the animation
            rootLayout.setVisibility(View.VISIBLE);
            circularReveal.start();
        } else {
            finish();
        }
    }

    protected void unRevealActivity() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            finish();
        } else {
            float finalRadius = (float) (Math.max(rootLayout.getWidth(), rootLayout.getHeight()) * 1.1);
            Animator circularReveal = ViewAnimationUtils.createCircularReveal(
                    rootLayout, revealX, revealY, finalRadius, 0);

            circularReveal.setDuration(400);
            circularReveal.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    rootLayout.setVisibility(View.INVISIBLE);
                    finish();
                }
            });


            circularReveal.start();
        }
    }
}
