package com.fullclient.bemobi.presentation.app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.fullclient.bemobi.R;
import com.fullclient.bemobi.injection.Injection;
import com.fullclient.bemobi.model.AppModel;
import com.fullclient.bemobi.model.Device;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ListAppsActivity extends AppCompatActivity implements ListAppsContract.View {

    private ListAppsPresenter listAppsPresenter;
    private ListAppsAdapter listAppsAdapter;
    private ProgressBar progressBar;
    private TextView error_msg;
    private RecyclerView rv_apps;

    private ArrayList<AppModel> items = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_apps);

        listAppsPresenter = new ListAppsPresenter(Injection.provideListApps(this), Schedulers.io(), AndroidSchedulers.mainThread());
        listAppsPresenter.attachView(this);

        progressBar = findViewById(R.id.progress_bar);
        error_msg = findViewById(R.id.error_msg);
        rv_apps = findViewById(R.id.rv_apps);
        listAppsAdapter = new ListAppsAdapter(null);
        LinearLayoutManager lm = new LinearLayoutManager(this);
        rv_apps.setLayoutManager(lm);
        rv_apps.setAdapter(listAppsAdapter);

        listAppsAdapter.setActivity(this);

        Device device = Device.getDevice();
        device.setDeviceCID("55");



        if (savedInstanceState == null) {
            listAppsPresenter.listApps(device);
        } else {
            listAppsPresenter.remountListApps(items = (ArrayList<AppModel>) savedInstanceState.getSerializable("arrapps"));
        }
    }

    @Override
    public void showResults(List<AppModel> apps) {
        rv_apps.setVisibility(View.VISIBLE);
        error_msg.setVisibility(View.GONE);
        items.addAll(apps);
        listAppsAdapter.setItems(apps);
    }

    @Override
    public void showError(String message) {
        error_msg.setVisibility(View.VISIBLE);
        rv_apps.setVisibility(View.GONE);
        error_msg.setText(message);
    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
        rv_apps.setVisibility(View.GONE);
        error_msg.setVisibility(View.GONE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
        rv_apps.setVisibility(View.VISIBLE);
        error_msg.setVisibility(View.GONE);
    }

    @Override
    protected void onDestroy() {
        listAppsPresenter.detachView();
        super.onDestroy();

    }

    @Override
    public void remountResults(List<AppModel> apps) {
        rv_apps.setVisibility(View.VISIBLE);
        error_msg.setVisibility(View.GONE);
        listAppsAdapter.setItems(apps);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable("arrapps", items);
        super.onSaveInstanceState(outState);
    }
}
