package com.fullclient.bemobi.presentation.app;

import com.fullclient.bemobi.data.AppsRepository;
import com.fullclient.bemobi.model.AppModel;
import com.fullclient.bemobi.model.Device;
import com.fullclient.bemobi.presentation.base.BasePresenter;

import java.util.List;

import rx.Scheduler;
import rx.Subscriber;

/**
 * Created by Gabriel on 08/07/2017.
 */

public class ListAppsPresenter extends BasePresenter<ListAppsContract.View> implements ListAppsContract.Presenter {
    private final Scheduler mainScheduler, ioScheduler;
    private AppsRepository appsRepository;

    ListAppsPresenter(AppsRepository userRepository, Scheduler ioScheduler, Scheduler mainScheduler) {
        this.appsRepository = userRepository;
        this.ioScheduler = ioScheduler;
        this.mainScheduler = mainScheduler;
    }

    @Override
    public void listApps(Device device) {
        checkViewAttached();
        getView().showLoading();

        addSubscription(appsRepository.listApps(device).subscribeOn(ioScheduler).observeOn(mainScheduler).subscribe(new Subscriber<List<AppModel>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                getView().hideLoading();
                getView().showError(e.getMessage());
            }

            @Override
            public void onNext(List<AppModel> apps) {
                getView().hideLoading();
                getView().showResults(apps);
            }
        }));
    }

    @Override
    public void remountListApps(List<AppModel> fruits) {
        checkViewAttached();
        getView().hideLoading();
        getView().remountResults(fruits);
    }
}