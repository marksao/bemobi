package com.fullclient.bemobi.presentation.app;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.fullclient.bemobi.R;
import com.fullclient.bemobi.model.AppModel;
import com.fullclient.bemobi.presentation.detailapp.DetailAppActivity;
import com.squareup.picasso.Picasso;

import java.util.List;


/**
 * Created by Gabriel on 09/07/2017.
 */

public class ListAppsAdapter extends RecyclerView.Adapter<AppsViewHolder> {

    private List<AppModel> items;
    private Context context;
    private Activity activity;

    public ListAppsAdapter(List<AppModel> items) {
        this.items = items;
    }

    @Override
    public AppsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_apps_list, parent, false);
        context = parent.getContext();
        return new AppsViewHolder(v);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(AppsViewHolder holder, int position) {
        final AppModel app = items.get(position);

        Picasso.get().load(app.getIcon()).into(holder.iv_icon);
        holder.app_name.setText(String.format("%s", app.getName()));

        holder.cv_item.setBackgroundColor(Color.parseColor(app.getColor()));
        holder.subtitle.setText(String.format("%s", app.getSubtitle()));
        //            Intent intent = new Intent(context, DetailAppActivity.class);
//            intent.putExtra("app", app);
//            context.startActivity(intent);
        holder.cv_item.setOnClickListener(o-> presentActivity(o,app));

//        holder.cv_item.setOnTouchListener(new View.OnTouchListener() {
//            public boolean onTouch(View v, MotionEvent event) {
//                int action = MotionEventCompat.getActionMasked(event);
//                switch (action){
//                    case (MotionEvent.ACTION_DOWN):
//                        presentActivity(v,app,event.getX(),event.getY());
//                        return true;
//                }
//                return true;
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    void setItems(List<AppModel> appList) {
        this.items = appList;
        this.notifyDataSetChanged();
    }

    public void presentActivity(View view, AppModel app) {
        ActivityOptionsCompat options = ActivityOptionsCompat.
                makeSceneTransitionAnimation(activity, view, "transition");
        int revealX = (int) (view.getX() + view.getWidth() / 2);
        int revealY = (int) (view.getY() + view.getHeight() / 2);

        Intent intent = new Intent(context, DetailAppActivity.class);
        intent.putExtra(DetailAppActivity.EXTRA_CIRCULAR_REVEAL_X, revealX);
        intent.putExtra(DetailAppActivity.EXTRA_CIRCULAR_REVEAL_Y, revealY);
        intent.putExtra("app",app);

        ActivityCompat.startActivity(context,intent, options.toBundle());
    }

    public void setActivity(Activity c){
        this.activity = c;
    }

}