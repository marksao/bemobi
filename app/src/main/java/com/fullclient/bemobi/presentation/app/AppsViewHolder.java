package com.fullclient.bemobi.presentation.app;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fullclient.bemobi.R;


/**
 * Created by Gabriel on 09/07/2017.
 */

public class AppsViewHolder extends RecyclerView.ViewHolder {

    public ImageView iv_icon;
    public TextView app_name;
    public TextView subtitle;
    public CardView cv_item;

    public AppsViewHolder(View itemView) {
        super(itemView);
        iv_icon = itemView.findViewById(R.id.iv_icon);
        app_name = itemView.findViewById(R.id.app_name);
        subtitle = itemView.findViewById(R.id.subtitle);
        cv_item = itemView.findViewById(R.id.cv_item);

    }
}
