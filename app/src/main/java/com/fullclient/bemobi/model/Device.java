package com.fullclient.bemobi.model;

/**
 * Created by medal on 3/16/2018.
 */

public class Device {

    private static Device state;
    private String DEVICE_COUNTRY_ID;
    private String DEVICE_SIM_ID;

    public static Device getDevice(){
        if(state==null){
            return state = new Device();
        }
        return state;
    }

    public String getCountryID() {
        return DEVICE_COUNTRY_ID;
    }

    public void setDeviceCID(String DEVICE_COUNTRY_ID) {
        this.DEVICE_COUNTRY_ID = DEVICE_COUNTRY_ID;
    }

    public void setDeviceSID(String DEVICE_SIM_ID) {
        this.DEVICE_SIM_ID = DEVICE_SIM_ID;
    }

    public String getId() {
        return DEVICE_SIM_ID;
    }
}
