package com.fullclient.bemobi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Gabriel on 10/07/2017.
 */

public class AppModel implements Serializable{

    @SerializedName("Name")
    @Expose
    private String name;

    @SerializedName("Banner")
    @Expose
    private String p_banner;

    @SerializedName("Version")
    @Expose
    private String version;

    @SerializedName("Color")
    @Expose
    private String color;

    @SerializedName("Icon")
    @Expose
    private String icon;

    @SerializedName("Subtitle")
    @Expose
    private String subtitle;

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getP_banner() {
        return p_banner;
    }

    public void setP_banner(String p_banner) {
        this.p_banner = p_banner;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
